package org.example;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class ResponseParser {
    public static void parseResponse(String response, boolean showTestCases) {
        Document doc = Jsoup.parse(response);

        // Check if the code compiled
        Elements compileErrors = doc.select("font[color=red]");
        if (!compileErrors.isEmpty()) {
            String compileErrorMessage = extractCompileErrorMessage(doc);
            displayCompileError(compileErrorMessage);
            return;
        }

        // Parse the result table
        Element table = doc.select("table.out").first();
        if (table != null) {
            displayResult(table, showTestCases);
        } else {
            System.out.println("No result found.");
        }
    }

    private static String extractCompileErrorMessage(Document doc) {
        Element preElement = doc.selectFirst("pre");
        if (preElement != null) {
            return preElement.text();
        }
        return "Unknown compile error";
    }

    private static void displayCompileError(String errorMessage) {
        // Display compile error with emoji
        System.out.println("\uD83D\uDEAB Compile error occurred. Please check your code.");
        System.out.println("   Error message: " + errorMessage);
    }

    private static void displayResult(Element table, boolean showTestCases) {
        // Display result table with emoji
        // Zombie code 😱
        // System.out.println("\uD83D\uDCD6 Result:");

        Elements rows = table.select("tr");
        int totalTests = 0;
        int passedTests = 0;
        for (Element row : rows) {
            Elements cells = row.select("td");
            if (cells.size() == 4) {
                totalTests++;
                String status = cells.get(2).text();
                if (status.equals("OK")) {
                    passedTests++;
                }
            }
        }

        if (passedTests == totalTests) {
            System.out.println("✅ All tests passed!");
        } else if (passedTests == 0) {
            System.out.println("❌ All tests failed!");
        } else {
            System.out.printf("⚠️ %d tests passed, %d tests failed\n", passedTests, totalTests - passedTests);
        }

        if (showTestCases) {
            for (Element row : rows) {
                Elements cells = row.select("td");
                if (cells.size() == 4) {
                    String expected = cells.get(0).text();
                    String run = cells.get(1).text();
                    String status = cells.get(2).text();
                    String emoji = cells.get(3).className().equals("ok") ? "✅" : "❌";
                    System.out.printf("   %s Expected: %s\n      Run: %s\n", emoji, expected, run);
                }
            }
        }
    }
}