package org.example;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.util.HashMap;
import java.util.Map;
import java.net.URLEncoder;

public class Main {

    private static final String CODINGBAT_RUN_URL = "https://codingbat.com/run";
    private static final String CODINGBAT_PROBLEM_URL = "https://codingbat.com/java/";

    public static void main(String[] args) {
        System.out.println("🤖 CodingBat Test Runner");
        // Parse command line arguments in a more robust way (Map for flags and List for arguments)
        Map<String, String> flags = new HashMap<>();
        for (String s : args) {
            if (s.startsWith("-")) {
                flags.put(s, s);
            }
        }
        String[] arguments = new String[args.length - flags.size()];
        int j = 0;
        for (String arg : args) {
            if (!arg.startsWith("-")) {
                arguments[j] = arg;
                j++;
            }
        }

        // Get verbose flag from command line arguments
        boolean verbose = flags.containsKey("-v") || flags.containsKey("--verbose");

        // Check for --watch flag
        if (flags.containsKey("--watch")) {
            String solutionsDirectory = "solutions";
            if (arguments.length > 0) {
                solutionsDirectory = arguments[0];
            }

            // Watch the solutions directory for changes
            watchSolutionsDirectory(solutionsDirectory, verbose);
        }

        // Check if the user has provided a specific problem path
        if (arguments.length > 0) {
            // If it is a directory, test all solutions in that directory
            if (Files.isDirectory(Paths.get(arguments[0]))) {
                testAllSolutions(arguments[0], verbose);
            } else {
                // Otherwise, test the solution at the given path
                testFilePath(arguments[0], verbose);
            }
        } else {
            // Otherwise, test all solutions
            testAllSolutions("solutions", verbose);
        }
    }

    private static void watchSolutionsDirectory(String solutionsDirectory, boolean verbose) {
        testAllSolutions(solutionsDirectory, verbose);
        try {
            WatchService watchService = FileSystems.getDefault().newWatchService();
            Path path = Paths.get(solutionsDirectory);
            path.register(watchService, StandardWatchEventKinds.ENTRY_MODIFY);

            System.out.println("👀 Watching " + solutionsDirectory + " for changes...");

            while (true) {
                WatchKey key = watchService.take();
                for (WatchEvent<?> event : key.pollEvents()) {
                    if (event.kind() == StandardWatchEventKinds.ENTRY_MODIFY) {
                        String directoryPath = solutionsDirectory + "/" + event.context();
                        // Print "Re-testing" to make it clear that the solution is being re-tested
                        System.out.println("🔁 Re-testing " + directoryPath + "...");
                        System.out.println("-----------------------------------------");
                        testAllSolutions(directoryPath, verbose);
                    }
                }
                key.reset();
            }
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }


    public static void testAllSolutions(String solutionsDirectory, boolean verbose) {
        try {
            Files.walk(Paths.get(solutionsDirectory))
                    .filter(Files::isRegularFile)
                    .forEach(filePath -> testFilePath(filePath.toString(), verbose));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void testFilePath(String filePath, boolean verbose) {
        String code = getCode(filePath);
        String problemName = getProblemName(filePath);
        String category = getCategory(filePath);
        String problemID = getProblemID(category, problemName);

        if (problemID != null) {
            // Using emoji to make the output more readable
            System.out.println("👉 Testing " + problemName + "...");

            Map<String, String> formData = new HashMap<>();
            formData.put("id", problemID);
            formData.put("code", code);
            formData.put("cuname", "");
            formData.put("owner", "");
            formData.put("adate", "20230601-212350z");

            String url = CODINGBAT_RUN_URL + "?" + encodeFormData(formData);
            String response = sendPostRequest(url);
            ResponseParser.parseResponse(response, verbose);
            System.out.println("-----------------------------------------");
        }
    }

    private static String encodeFormData(Map<String, String> formData) {
        StringBuilder sb = new StringBuilder();
        for (Map.Entry<String, String> entry : formData.entrySet()) {
            String key = URLEncoder.encode(entry.getKey(), StandardCharsets.UTF_8);
            String value = URLEncoder.encode(entry.getValue(), StandardCharsets.UTF_8);
            sb.append(key).append("=").append(value).append("&");
        }
        return sb.toString();
    }

    private static String getProblemID(String category, String problemName) {
        String categoryURL = CODINGBAT_PROBLEM_URL + category;
        try {
            Document doc = Jsoup.connect(categoryURL).get();
            Elements anchorTags = doc.select("a[href]");
            for (Element anchorTag : anchorTags) {
                String innerText = anchorTag.text();
                String href = anchorTag.attr("href");
                if (innerText.equals(problemName) && href.startsWith("/prob/p")) {
                    return href.replace("/prob/", "");
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static String getCode(String filePath) {
        try {
            Path path = Paths.get(filePath);
            return new String(Files.readAllBytes(path));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static String getProblemName(String filePath) {
        String[] parts = filePath.split("/");
        if (parts.length >= 3) {
            String fileName = parts[parts.length - 1];
            int extensionIndex = fileName.lastIndexOf('.');
            if (extensionIndex != -1) {
                return fileName.substring(0, extensionIndex);
            }
        }
        return null;
    }

    private static String getCategory(String filePath) {
        String[] parts = filePath.split("/");
        if (parts.length >= 2) {
            return parts[1];
        }
        return null;
    }

    private static String sendPostRequest(String url) {
        StringBuilder response = new StringBuilder();

        try {
            URL obj = new URL(url);
            HttpURLConnection connection = (HttpURLConnection) obj.openConnection();

            connection.setRequestMethod("POST");
            connection.setRequestProperty("User-Agent", "Mozilla/5.0");
            connection.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

            int responseCode = connection.getResponseCode();

            if (responseCode == HttpURLConnection.HTTP_OK) {
                try (BufferedReader in = new BufferedReader(
                        new InputStreamReader(connection.getInputStream()))) {
                    String inputLine;
                    while ((inputLine = in.readLine()) != null) {
                        response.append(inputLine);
                        response.append("\n");
                    }
                }
            } else {
                System.out.println("POST request failed. Response Code: " + responseCode);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return response.toString();
    }
}
