public String frontTimes(String str, int n) {
    String result = "";
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < 3 && j < str.length(); j++) {
            result += str.charAt(j);
        }
    }
    return result;
}
