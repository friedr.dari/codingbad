public int[] fix34(int[] nums) {
    int index4 = 0;

    for (int i = 0; i < nums.length - 1; i++) {
        if (nums[i] == 3) {
            while (index4 < nums.length && (nums[index4] != 4 || (index4 > 0 && nums[index4 - 1] == 3))) {
                index4++;
            }

            if (index4 < nums.length) {
                int temp = nums[i + 1];
                nums[i + 1] = nums[index4];
                nums[index4] = temp;
            }
        }
    }
    return nums;
}
