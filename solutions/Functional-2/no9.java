public List<Integer> no9(List<Integer> nums) {
    return nums.stream()
        .filter(number -> number % 10 != 9)
        .collect(Collectors.toList());
}
