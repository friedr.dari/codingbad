public List<Integer> noNeg(List<Integer> nums) {
    return nums.stream()
        .filter(number -> number >= 0)
        .collect(Collectors.toList());
}
