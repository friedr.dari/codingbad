public boolean groupSum6(int start, int[] nums, int target) {
      // Base case: if we've reached the end of the array, check if we've hit the target
    if (start >= nums.length) {
        return target == 0;
    }

    // If the current number is a 6, we must include it in the group
    if (nums[start] == 6) {
        return groupSum6(start + 1, nums, target - nums[start]);
    }

    // Otherwise, we have two options: include the current number or skip it
    return groupSum6(start + 1, nums, target - nums[start]) || groupSum6(start + 1, nums, target);
}
