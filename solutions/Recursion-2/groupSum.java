public boolean groupSum(int start, int[] nums, int target) {
      if (start >= nums.length) { // base case: reached end of array
        return (target == 0);
    }
    if (groupSum(start + 1, nums, target - nums[start])) { // include current element
        return true;
    }
    if (groupSum(start + 1, nums, target)) { // exclude current element
        return true;
    }
    return false; // neither including nor excluding current element leads to target sum
}
