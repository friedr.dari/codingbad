public String withoutString(String base, String remove) {
    String result = "";
    int index = 0;
    int removeLength = remove.length();
    while (index < base.length()) {
        int indexOfRemove = base.toLowerCase().indexOf(remove.toLowerCase(), index);
        if (indexOfRemove == -1) {
            result += base.substring(index);
            break;
        }
        result += base.substring(index, indexOfRemove);
        index = indexOfRemove + removeLength;
    }
    return result;
}
