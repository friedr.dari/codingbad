public Map<String, Integer> word0(String[] strings) {
    Map<String, Integer> result = new HashMap<String, Integer>();
    for (String s : strings) {
        result.put(s, 0);
    }
    return result;
}
