public boolean makeBricks(int small, int big, int goal) {
  int bigBricksNeeded = goal / 5;
    if (bigBricksNeeded <= big) {
        int remainingGoal = goal - (bigBricksNeeded * 5);
        return small >= remainingGoal;
    } else {
        int remainingGoal = goal - (big * 5);
        return small >= remainingGoal;
    }
}