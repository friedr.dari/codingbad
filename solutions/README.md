# 🦄 Codingbad

This repository contains my solutions to the problems from the [Codingbat](https://codingbat.com/java) website and a tool for testing them.

## Solutions
The solutions are in the `solutions` directory. The directory structure is as follows:
```
solutions/[category]/[problem].java
```
where `[category]` is the category of the problem and `[problem]` is the name of the problem.

## Tester
To run the tester using maven, use the following command:
```bash
mvn install
mvn exec:java
```

### Arguments
The tester accepts the following arguments:
```
usage: [file/directory] [options]
 -v,--verbose             print the output of the tests
    --watch               watch the solutions directory for changes
```

### Examples
```bash
# Test a single file
mvn exec:java -Dexec.args="solutions/Map-2/word0.java"

# Test a directory
mvn exec:java -Dexec.args="solutions/Map-2"

# Test a directory and print the output of the tests
mvn exec:java -Dexec.args="solutions/Map-2 -v"

# Test all files and watch for changes
mvn exec:java -Dexec.args="--watch"
```
