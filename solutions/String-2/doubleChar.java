public String doubleChar(String str) {
    StringBuilder doubled = new StringBuilder();
    for (int i = 0; i < str.length(); i++) {
        char c = str.charAt(i);
        doubled.append(c).append(c);
    }
    return doubled.toString();
}
